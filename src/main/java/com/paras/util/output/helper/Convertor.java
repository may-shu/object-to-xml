package com.paras.util.output.helper;

/**
 * Convertor class to convert a object to string.
 * @author Paras
 *
 */
public class Convertor {

	/**
	 * Convert object's value to string.
	 * @param object
	 */
	public static String toString( Object object ) {
		String value = null;
		
		if( object instanceof String ) {
			value = ( String ) object;
		} else if ( object instanceof Integer ) {
			value = String.valueOf( (Integer ) object);
		} else if ( object instanceof Boolean ) {
			value = String.valueOf( (Boolean) object );
		}
		
		return value;
	}
	
}

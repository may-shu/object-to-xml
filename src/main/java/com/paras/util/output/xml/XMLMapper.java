package com.paras.util.output.xml;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;

import com.paras.util.output.helper.Convertor;
import com.paras.util.output.xml.annotation.Element;
import com.paras.util.output.xml.annotation.Embedded;
import com.paras.util.output.xml.annotation.Root;

/**
 * Model an object into XML.
 * @author Paras
 *
 */
public class XMLMapper {
	
	/**
	 * Class on which we have to operate.
	 */
	private Class<? extends Object> clazz;
	
	/**
	 * Object to create XML.
	 */
	private Object instance;
	
	/**
	 * List of instances to operate on.
	 */
	private List<? extends Object> list;

	private static Logger LOGGER = Logger.getLogger( XMLMapper.class );
	
	/**
	 * Private constructor to ensure that we have a class to work upon.
	 * @param clazz
	 */
	private XMLMapper( Class<? extends Object> clazz ) {
		this.clazz = clazz;
	}
	
	/**
	 * Get a XMLMapper instance.
	 */
	public static XMLMapper of( Class<? extends Object> clazz ) {
		return new XMLMapper( clazz );
	}
	
	/**
	 * Create Operator on these.
	 */
	public XMLMapper using( Object instance ) {
		
		if( instance != null ) {
			if( instance instanceof List<?> ) {
				usingList( (List<?>) instance );
			}
			if( this.clazz.isAssignableFrom( instance.getClass()) ) {
				this.instance = instance;
			}		
		}
		return this;
	}
	
	/**
	 * Operate on this list.
	 */
	private XMLMapper usingList( List<? extends Object> list ) {
		this.list = list;
		return this;
	}
	
	/**
	 * Generate XML for passed object.
	 * @return
	 */
	public String xml() {
		if( instance != null ) {
			return xml( this.instance ).toString();
		}
		
		if( list != null ) {
			return xml( this.list ).toString();
		}
		
		return null;
		
	}
	
	/**
	 * Turn List into XML.
	 */
	private StringBuffer xml( List<? extends Object> list ) {
		
		StringBuffer buffer = new StringBuffer();
		
		/*
		 * If list is null, or emptry.
		 * Return blank list.
		 */
		if( list == null || list.isEmpty() ) {
			
			buffer = buffer.append( "<list></list>" );
			
		} 
		/*
		 * If list has just one element.
		 */
		else if( list.size() == 1 ) {
			Object object = list.get( 0 );
			buffer = buffer.append( xml( object ));
		} else {

			/*
			 * Create a list.
			 */
			buffer.append( "<list>" );
			
			for( Object object : list ) {
				buffer.append( xml( object ));
			}
			
			buffer.append( "</list>" );
			
		}
		
		return buffer;
	}
	
	/**
	 * Generate XML for instance.
	 */
	@SuppressWarnings("rawtypes")
	private StringBuffer xml( Object object ) {
		
		if( object == null ) {
			return new StringBuffer( "" );
		}
		
		/*
		 * We will work with StringBuffer, as it is thread safe.
		 */
		StringBuffer buffer = new StringBuffer();
		
		/*
		 * We will also be working only when class definition is annotated with annotation root.
		 */
		if( clazz.isAnnotationPresent( Root.class )) {			
			
			/*
			 * Let's get all fields.
			 */
			Field[] fields = clazz.getDeclaredFields();
			
			/*
			 * If there are fields to work with,
			 * we will create an empty container tag. 
			 */
			if( fields.length > 0 ) {
				buffer.append( "<" + clazz.getAnnotation( Root.class ).name() + ">" );
			}
			
			for( Field field : fields ) {
				
				/*
				 * We will process each of the field only when it is annotated with Element annotation.
				 * Being annotated with annotation Element, allows us to get a name with which data will be written.
				 */
				if( field.isAnnotationPresent( Element.class ) || field.isAnnotationPresent( Embedded.class )) {
					
					/*
					 * Configured Name.
					 */
					String element = field.getAnnotation( Element.class ).name();
					
					/*
					 * Getter method name.
					 */
					String getter = "get" + toFirstLetterCapital( field.getName() );
					
					try {
						
						/*
						 * Retrieve getter method.
						 * And execute it.
						 * And, retrieve value calculated.
						 * And, change it to string.
						 * And, convert it into XML tag.
						 * And, append it to buffer.
						 * Simple.
						 */
						
						
						Method method = clazz.getDeclaredMethod( getter );
						Object result = null;
						String value = null;
						
						if (Collection.class.isAssignableFrom(field.getType())) {
							/**
							 * Let's check if this field is annotated with Annotation Embedded.
							 * That can ease things up.
							 */
							if( field.isAnnotationPresent( Embedded.class )) {
								result = method.invoke( object );
								
								if( result != null ) {
									buffer.append( "<" + element + ">" );								
								
									Class<? extends Object> embeddedClass = field.getAnnotation(Embedded.class ).embedded();
									for( Object res : (Collection)result ) {
										String xml = XMLMapper.of( embeddedClass )
												.using( res )
												.xml();
										
										buffer.append( xml );
									}
									buffer.append( "</" + element + ">" );
								}
								
								
							}
						} else {
							/**
							 * Continue See, what can be done.
							 */
							result = method.invoke( object );							
							value = Convertor.toString( result );
							buffer.append( getTag( element, value ));
						}					
						
					} catch (NoSuchMethodException ex) {
						
						LOGGER.error( "In XMLMapper | In xml | Caught NoSuchMethodException | " + ex.getMessage());
						ex.printStackTrace();
						
					} catch (SecurityException ex) {
						
						LOGGER.error( "In XMLMapper | In xml | Caught SecurityException | " + ex.getMessage());
						ex.printStackTrace();
						
					} catch (IllegalAccessException ex) {

						LOGGER.error( "In XMLMapper | In xml | Caught IllegalAccessException | " + ex.getMessage());
						ex.printStackTrace();
						
					} catch (IllegalArgumentException ex) {

						LOGGER.error( "In XMLMapper | In xml | Caught IllegalArgumentException | " + ex.getMessage());
						ex.printStackTrace();
						
					} catch (InvocationTargetException ex) {

						LOGGER.error( "In XMLMapper | In xml | Caught InvocationTargetException | " + ex.getMessage());
						ex.printStackTrace();
						
					}
				}
				
			}
			
			/*
			 * Let's close the buffer here.
			 */
			if( fields.length > 0 ) {
				buffer.append( "</" + clazz.getAnnotation( Root.class ).name() + ">" );
			}
		}
		
		return buffer;
	}
	
	/**
	 * Make first letter capital.
	 */
	private static String toFirstLetterCapital( String str ) {
		return str.substring(0, 1).toUpperCase() + str.substring( 1 );
	}
	
	private StringBuffer getTag( String element, String value ) {
		StringBuffer buffer = new StringBuffer();
		
		if( value == null ) {
			return buffer;
		}
		
		buffer.append( "<" + element + ">" );
		buffer.append( value );
		buffer.append( "</" + element + ">" );
		
		return buffer;
	}
}

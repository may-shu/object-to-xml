package com.paras.util.output.xml.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to identify a class name as root element of xml tag.
 * @author Paras
 *
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface Root {

	/**
	 * Name of Root Tag.
	 *
	 */	
	public String name() default "root";
	
}

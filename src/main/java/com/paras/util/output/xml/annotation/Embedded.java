package com.paras.util.output.xml.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to identify a field as embedded class.
 * @author Paras
 *
 */
@Retention( RetentionPolicy.RUNTIME )
public @interface Embedded {
	
	/**
	 * Embedded class Type.
	 */
	public Class<? extends Object> embedded();
	
}

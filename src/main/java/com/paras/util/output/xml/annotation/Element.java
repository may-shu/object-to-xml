package com.paras.util.output.xml.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that identifies an attribute to be used as a element.
 * @author Paras
 *
 */
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Element {
	/**
	 * Name of attribute.
	 */
	public String name();
}